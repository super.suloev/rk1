# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.26

# Default target executed when no arguments are given to make.
default_target: all
.PHONY : default_target

# Allow only one "make -f Makefile2" at a time, but pass parallelism.
.NOTPARALLEL:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/skat45/CLionProjects/BC

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/skat45/CLionProjects/BC

#=============================================================================
# Targets provided globally by CMake.

# Special rule for the target edit_cache
edit_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake cache editor..."
	/usr/bin/ccmake -S$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : edit_cache

# Special rule for the target edit_cache
edit_cache/fast: edit_cache
.PHONY : edit_cache/fast

# Special rule for the target rebuild_cache
rebuild_cache:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --cyan "Running CMake to regenerate build system..."
	/usr/bin/cmake --regenerate-during-build -S$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR)
.PHONY : rebuild_cache

# Special rule for the target rebuild_cache
rebuild_cache/fast: rebuild_cache
.PHONY : rebuild_cache/fast

# The main all target
all: cmake_check_build_system
	$(CMAKE_COMMAND) -E cmake_progress_start /home/skat45/CLionProjects/BC/CMakeFiles /home/skat45/CLionProjects/BC//CMakeFiles/progress.marks
	$(MAKE) $(MAKESILENT) -f CMakeFiles/Makefile2 all
	$(CMAKE_COMMAND) -E cmake_progress_start /home/skat45/CLionProjects/BC/CMakeFiles 0
.PHONY : all

# The main clean target
clean:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/Makefile2 clean
.PHONY : clean

# The main clean target
clean/fast: clean
.PHONY : clean/fast

# Prepare targets for installation.
preinstall: all
	$(MAKE) $(MAKESILENT) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall

# Prepare targets for installation.
preinstall/fast:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/Makefile2 preinstall
.PHONY : preinstall/fast

# clear depends
depend:
	$(CMAKE_COMMAND) -S$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 1
.PHONY : depend

#=============================================================================
# Target rules for targets named BC

# Build rule for target.
BC: cmake_check_build_system
	$(MAKE) $(MAKESILENT) -f CMakeFiles/Makefile2 BC
.PHONY : BC

# fast build rule for target.
BC/fast:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/BC.dir/build.make CMakeFiles/BC.dir/build
.PHONY : BC/fast

functions.o: functions.c.o
.PHONY : functions.o

# target to build an object file
functions.c.o:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/BC.dir/build.make CMakeFiles/BC.dir/functions.c.o
.PHONY : functions.c.o

functions.i: functions.c.i
.PHONY : functions.i

# target to preprocess a source file
functions.c.i:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/BC.dir/build.make CMakeFiles/BC.dir/functions.c.i
.PHONY : functions.c.i

functions.s: functions.c.s
.PHONY : functions.s

# target to generate assembly for a file
functions.c.s:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/BC.dir/build.make CMakeFiles/BC.dir/functions.c.s
.PHONY : functions.c.s

game.o: game.c.o
.PHONY : game.o

# target to build an object file
game.c.o:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/BC.dir/build.make CMakeFiles/BC.dir/game.c.o
.PHONY : game.c.o

game.i: game.c.i
.PHONY : game.i

# target to preprocess a source file
game.c.i:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/BC.dir/build.make CMakeFiles/BC.dir/game.c.i
.PHONY : game.c.i

game.s: game.c.s
.PHONY : game.s

# target to generate assembly for a file
game.c.s:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/BC.dir/build.make CMakeFiles/BC.dir/game.c.s
.PHONY : game.c.s

main.o: main.c.o
.PHONY : main.o

# target to build an object file
main.c.o:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/BC.dir/build.make CMakeFiles/BC.dir/main.c.o
.PHONY : main.c.o

main.i: main.c.i
.PHONY : main.i

# target to preprocess a source file
main.c.i:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/BC.dir/build.make CMakeFiles/BC.dir/main.c.i
.PHONY : main.c.i

main.s: main.c.s
.PHONY : main.s

# target to generate assembly for a file
main.c.s:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/BC.dir/build.make CMakeFiles/BC.dir/main.c.s
.PHONY : main.c.s

validate.o: validate.c.o
.PHONY : validate.o

# target to build an object file
validate.c.o:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/BC.dir/build.make CMakeFiles/BC.dir/validate.c.o
.PHONY : validate.c.o

validate.i: validate.c.i
.PHONY : validate.i

# target to preprocess a source file
validate.c.i:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/BC.dir/build.make CMakeFiles/BC.dir/validate.c.i
.PHONY : validate.c.i

validate.s: validate.c.s
.PHONY : validate.s

# target to generate assembly for a file
validate.c.s:
	$(MAKE) $(MAKESILENT) -f CMakeFiles/BC.dir/build.make CMakeFiles/BC.dir/validate.c.s
.PHONY : validate.c.s

# Help Target
help:
	@echo "The following are some of the valid targets for this Makefile:"
	@echo "... all (the default if no target is provided)"
	@echo "... clean"
	@echo "... depend"
	@echo "... edit_cache"
	@echo "... rebuild_cache"
	@echo "... BC"
	@echo "... functions.o"
	@echo "... functions.i"
	@echo "... functions.s"
	@echo "... game.o"
	@echo "... game.i"
	@echo "... game.s"
	@echo "... main.o"
	@echo "... main.i"
	@echo "... main.s"
	@echo "... validate.o"
	@echo "... validate.i"
	@echo "... validate.s"
.PHONY : help



#=============================================================================
# Special targets to cleanup operation of make.

# Special rule to run CMake to check the build system integrity.
# No rule that depends on this can have commands that come from listfiles
# because they might be regenerated.
cmake_check_build_system:
	$(CMAKE_COMMAND) -S$(CMAKE_SOURCE_DIR) -B$(CMAKE_BINARY_DIR) --check-build-system CMakeFiles/Makefile.cmake 0
.PHONY : cmake_check_build_system

