#include "functions.h"

void draw_lines(char *text[]) {
    clear();
    for (unsigned int i = 0; i < WINDOW_H/2; i++) {
        move(i*2, 0);
        printw("%s", text[i]);
    }
}