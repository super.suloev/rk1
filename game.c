#include "game.h"

void game(params input) {
    input.start_time = time(NULL);
    start_color();
    init_pair(RED_TEXT, COLOR_RED, COLOR_BLACK);
    init_pair(GREEN_TEXT, COLOR_GREEN, COLOR_BLACK);
    char text[WINDOW_H/2][WINDOW_W];
    FILE *file;
    unsigned int mistakes_counter = 0;

    switch (input.difficultly) {
        case 'e':
            file = fopen(EASY_FILE, "r");
            break;

        case 'm':
            file = fopen(MEDIUM_FILE, "r");
            break;

        case 'h':
            file = fopen(HARD_FILE, "r");
            break;
    }

    unsigned int i = 0; // NUMBER OF LAST READ STRING IN FILE
    unsigned int char_num; // NUMBER OF CHAR THAT USER MUST ENTER
    short str_num = 0; // NUMBER OF STRING ON THE SCREEN ON WHICH USER LOCATED
    short file_was_read_all = 0;

    // COUNT TOTAL NUMBER OF SYMBOLS IN FILE
    unsigned int total_num_of_chars = 0;
    char c;
    for (c = getc(file); c != EOF; c = getc(file))
        total_num_of_chars++;
    input.total_chars = total_num_of_chars;
    fseek(file, 0, SEEK_SET); // POINTER OFFSET TO BEGINNING FILE

    while (1) { // READING WINDOW_H/2 FIRST STRINGS FROM FILE AND PRINT THEM
        if (i + 1 > WINDOW_H/2)
            break;
        if (fgets(text[i], WINDOW_W, file) == NULL) {
            file_was_read_all = 1;
            break;
        }
        move(i*2, 0);
        printw("%s", text[i]);
        i++;
    }

    while (1) {
        for (char_num = 0; char_num < strlen(text[str_num]); char_num++) {
            attron(COLOR_PAIR(RED_TEXT));
            move(WINDOW_H-1, WINDOW_W/3*2);
            printw("%d/%d", mistakes_counter, input.num_of_mistakes);
            move(WINDOW_H-1, WINDOW_W/2);
            printw("%ld", time(NULL) - input.start_time);
            attroff(COLOR_PAIR(RED_TEXT));

            mvaddch(str_num*2 + 1, char_num, '^');
            char char_input = getch();
            if (time(NULL) - input.start_time >= input.time) print_result(input, mistakes_counter);
            if (char_input == text[str_num][char_num] || (char_input == ' ' && text[str_num][char_num] == '\n')) {
                attron(COLOR_PAIR(GREEN_TEXT));
                mvaddch(str_num*2, char_num, text[str_num][char_num]);
                attroff(COLOR_PAIR(GREEN_TEXT));
                input.num_of_regular_sym++;
            }
            else {
                attron(COLOR_PAIR(RED_TEXT));
                mvaddch(str_num*2, char_num, text[str_num][char_num]);
                attroff(COLOR_PAIR(RED_TEXT));
                input.num_of_eregular_sym++;
                if (++mistakes_counter >= input.num_of_mistakes)
                    print_result(input, mistakes_counter);
            }
        }

        if (file_was_read_all) {
            str_num++;
            if (str_num >= WINDOW_H/2)
                break;
        } else {
            char new_line[WINDOW_W];
            if (fgets(new_line, WINDOW_W, file) == NULL) {
                file_was_read_all = 1;
                str_num++;
            }
            if (!file_was_read_all) {
                for (short j = 0; j < WINDOW_H / 2 - 1; j++)
                    for (unsigned int k = 0; k < WINDOW_W; k++)
                        text[j][k] = text[j + 1][k];

                for (unsigned int k = 0; k < WINDOW_W; k++)
                    text[WINDOW_H / 2 - 1][k] = new_line[k];
            }
        }

        clear();
        for (unsigned int j = 0; j < WINDOW_H/2; j++) {
            move(j*2, 0);
            printw("%s", text[j]);
        }

    }

    fclose(file);

    print_result(input, mistakes_counter);
}

void print_result(params input, unsigned int mistakes) {
    clear();
    mvprintw(0, 0, "Game over!");
    mvprintw(1, 0, "The number of correctly entered: \t%u", input.num_of_regular_sym);
    mvprintw(2, 0, "The number of not correctly entered: \t%u", input.num_of_eregular_sym);
    mvprintw(3, 0, "Total number of characters entered: \t%u of %u", input.num_of_eregular_sym
        + input.num_of_regular_sym, input.total_chars);
    mvprintw(4, 0, "Game time: \t%ld", time(NULL) - input.start_time);
    mvprintw(5, 0, "Your speed: \t%ld symbols per second", (input.num_of_regular_sym) / (time(NULL) - input.start_time));
    getch();
    wait();
    endwin();
    exit(0);
}

void wait() {
    sleep(7);
}
