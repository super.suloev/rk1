#ifndef BC_GAME_H
#define BC_GAME_H

#include <ncurses.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include "parameters.h"
#include "functions.h"

#define RED_TEXT 2
#define GREEN_TEXT 3

#define EASY_FILE "/home/skat45/CLionProjects/BC/texts/easy.txt"
#define MEDIUM_FILE "/home/skat45/CLionProjects/BC/texts/medium.txt"
#define HARD_FILE "/home/skat45/CLionProjects/BC/texts/hard.txt"

void game(params input);
void print_result(params input, unsigned int  mistakes);
void wait();

#endif
