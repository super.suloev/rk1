#include "main.h"

int main (int argc, char *argv[]) {
    // VALIDATE INPUT PARAMETERS && WRITE THAT TO STRUCTURE
    params input;
    input.num_of_regular_sym = 0;
    input.num_of_eregular_sym = 0;
    if (!validate_args(argc, argv, &input))
        return ERROR_INPUT_CODE;

    // NCOURCES SETTINGS
    initscr();
    noecho();
    curs_set(0); // REMOVE SYSTEM POINTER
    refresh();
    game(input);
}