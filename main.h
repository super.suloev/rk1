#ifndef BC_MAIN_H
#define BC_MAIN_H

#include <ncurses.h>
#include <string.h>
#include "validate.h"
#include "parameters.h"

#define ERROR_INPUT_CODE 123

int main (int argc, char *argv[]);

#endif
