#ifndef BC_PARAMETERS_H
#define BC_PARAMETERS_H

#define MIN_TIME 10
#define MAX_TIME 180

typedef struct {
    char difficultly;
    long time;
    short num_of_mistakes;
    unsigned int num_of_regular_sym;
    unsigned int num_of_eregular_sym;
    long int start_time;
    unsigned int total_chars;
} params;

#define WINDOW_H 20
#define WINDOW_W 265

#endif