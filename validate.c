#include "validate.h"

short validate_args(int argc, char *argv[], params *input) {
    if (argc != 4) {
        printf("Number of input arguments must be equals three!");
        return 0;
    }

    if (strcmp(argv[1], "easy") + strcmp(argv[1], "medium") + strcmp(argv[1], "hard") == 3) {
        printf("First argument must be \"easy\" or \"medium\" or \"hard\"");
        return 0;
    }

    long time = strtol(argv[2], NULL, 10);
    if (time < MIN_TIME || time > MAX_TIME) {
        printf("Second argument must be a digit from %ld to %ld", MIN_TIME, MAX_TIME);
        return 0;
    }

    long mistakes_num = strtol(argv[3], NULL, 10);
    if (mistakes_num < 0) {
        printf("Third argument must be a digit bigger then zero");
        return 0;
    }

    input->time = time;
    input->num_of_mistakes = mistakes_num;
    input->difficultly = argv[1][0];

    return 1;
}