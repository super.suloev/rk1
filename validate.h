#ifndef BC_VALIDATE_H
#define BC_VALIDATE_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "parameters.h"


short validate_args(int argc, char *argv[], params *input);

#endif
